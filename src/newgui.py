#!/usr/bin/env python3

import gi
# use gtk 3
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

# custom window defined
class MyWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Hello World")

        self.button = Gtk.Button(label="Click Here")
        self.button.connect("clicked", self.on_button_clicked)
        self.add(self.button)

    def on_button_clicked(self, widget):
        print("Hello World")

# create window
win = MyWindow()
# link "X" to close window
win.connect("destroy", Gtk.main_quit)
# show window
win.show_all()
# main loop
Gtk.main()
